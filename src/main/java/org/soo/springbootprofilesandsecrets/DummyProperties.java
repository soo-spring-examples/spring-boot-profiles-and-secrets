package org.soo.springbootprofilesandsecrets;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Collection;

@ConfigurationProperties("org.soo.dummy")
public record DummyProperties(
        String title,
        String version,
        String database,
        String databaseSecret,
        String applicationSecret,
        Collection<String> comments
) {
}
