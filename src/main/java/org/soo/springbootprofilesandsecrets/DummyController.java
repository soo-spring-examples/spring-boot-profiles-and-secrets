package org.soo.springbootprofilesandsecrets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/dummy")
public class DummyController {

    public static final String ACTIVE_PROFILES = "activeProfiles";

    @Autowired
    private Environment environment;

    private final DummyProperties dummyProperties;


    public DummyController(DummyProperties dummyProperties) {
        this.dummyProperties = dummyProperties;
    }

    @GetMapping
    public Map<String, Object> printProps() {

        String[] activeProfiles = this.environment.getActiveProfiles();

        return Map.of(
                "title", dummyProperties.title(),
                "version", dummyProperties.version(),
                "database", dummyProperties.database(),
                "databaseSecret", dummyProperties.databaseSecret(),
                "applicationSecret", dummyProperties.applicationSecret(),
                "comments", String.valueOf(dummyProperties.comments()),
                ACTIVE_PROFILES, activeProfiles
        );
    }
}
